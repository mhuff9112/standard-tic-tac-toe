def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board,current_player):
    print_board(board)
    print("GAME OVER!\n", current_player, "has won")
    exit()

def is_top_row_winner(board):
    state = False
    if board[0] == board[1] and board[1] == board[2]:
        state = True
    return state  
    
def is_middle_row_winner(board):
    state = False
    if board[3] == board[4] and board[4] == board[5]:
        state = True
    return state   
    
def is_bottom_row_winner(board):
    if board[6] == board[7] and board[7] == board[8]:
        return True   

def is_left_column_winner(board):
    if board[0] == board[3] and board[3] == board[6]:
        return True

def is_middle_column_winner(board):
    if board[1] == board[4] and board[4] == board[7]:
        return True
    
def is_right_column_winner(board):
    if board[2] == board[5] and board[5] == board[8]:
        return True
    
def is_right_diagonal_winner(board):
    if board[0] == board[4] and board[4] == board[8]:
        return True
def is_left_diagonal_winner(board):
    if board[2] == board[4] and board[4] == board[6]:
        return True

  
board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_top_row_winner(board) == True:
        game_over(board,current_player)
    
    elif is_middle_row_winner(board) == True:
        game_over(board,current_player)
    
    elif is_bottom_row_winner(board)== True:
        game_over(board,current_player)
        
    elif is_left_column_winner(board)== True:
        game_over(board,current_player)
        
    elif is_middle_column_winner== True:
        game_over(board,current_player)
        
    elif is_right_column_winner(board)== True:
        game_over(board,current_player)
        
    elif is_right_diagonal_winner(board)== True:
        game_over(board,current_player)
        
    elif is_left_diagonal_winner(board)== True:
        game_over(board,current_player)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
